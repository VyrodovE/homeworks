describe('multiplyOrSum', function () {
    it('should return sum', function () {

        const a = 1;
        const b = 2;
        const expected =3;

        const actual = multiplyOrSum(a,b);

        assert.equal(actual,expected);
    });

    it('should return a * b', function () {
        const a =2;
        const b=3;
        const expected =6;
        const actual=multiplyOrSum(a,b);
        assert.equal(actual,expected);
    })
});

describe('getQuarterByCoordinates', function () {
    it('should return quarter by coordinates', function () {
        const testData = [
            {x:1, y:2, expected:1},
            {x:-1, y:1, expected:2}
        ];
        testData.forEach(function(data){
            const {x,y,expected}=data;
            it('should return ${expected} when x = ${x}, y={y}', function () {
                const actual = getQuarterByCoordinates(x,y);
                assert.strictEqual(actual,expected)
            });
        });
    });

    describe('throw Error when point is on axis',function () {
        const testData = [
            {x:0, y:1},
        ];

        testData.forEach(function(data){
            const {x,y} = data;
            it('should return ${expected} when x = ${x}, y={y}', function (){

            }
        })
    })

    it('should return 2', function () {
        const x =-1;
        const y =1;
        const expected =2;
        const actual = getQuarterByCoordinates(x,y);
        assert.strictEqual(actual,expected);
    });

});

describe ('calculateMaxSUmOrMultiplyPlusThree', function () {
    describe('returns max of a+b+c or a*b*c plus 3', function () {
        const testData=[
            {a:0,b:1,c:2, expected:6},
        ];
        testData.forEach(function (data){
            it ('should return ${expected} when a = ${a}, b=${b}, c = ${c}', function());
            const{a,b,c,expected}=data;
            const actual = calculateMaxSUmOrMultiplyPlusThree(a,b,c);
            assert.strictEqual(actual,expected);
        });
    });
});