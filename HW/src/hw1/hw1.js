function multiplyOrSum(a,b){
    if (a%2 ===0){
        return a*b;
    }
    return a+b;
}

function getQuarterByCoordinates(x,y){
    if (x > 0 && y >0){
        return 1;
    }
    else  if (x<0 && y>0) {
        return 2;
    }
    else  if (x<0 && y<0) {
        return 3;
    }
    else  if (x>0 && y<0) {
        return 4;
    }
    else{
        throw Error('Point is no axis');
    }
}

function getPositiveNumbersSum(a,b,c){
    let result = 0;
    if(a>0){
        result +=a;
    }

    if(b>0){
        result +=b;
    }

    if(c>0){
        result +=c;
    }

    return result;
}